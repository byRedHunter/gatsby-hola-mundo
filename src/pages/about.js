import React from "react"
import SEO from "../components/seo"
import Header from "../components/header"

const About = () => {
  return (
    <>
      <SEO title="About" />
      <Header title="About" />

      <main>Acerca de nosotros</main>
    </>
  )
}

export default About
