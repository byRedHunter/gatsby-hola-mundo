import React from "react"

import SEO from "../components/seo"
import Header from "../components/header"

const IndexPage = () => (
  <>
    <SEO title="Home" />
    <Header title="Home" />

    <main>Main del Home</main>
  </>
)

export default IndexPage
