import React from "react"
import SEO from "../components/seo"
import Header from "../components/header"

const products = () => {
  return (
    <>
      <SEO title="Products" />
      <Header title="Products" />

      <main>Aqui van los productos</main>
    </>
  )
}

export default products
