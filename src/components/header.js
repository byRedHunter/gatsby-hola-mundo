import React from "react"
import { Link } from "gatsby"

import "./header.css"

const header = ({ title }) => {
  return (
    <header>
      <nav>
        <Link to="/" className="link">
          <h1>Home</h1>
        </Link>
        <Link to="/products" className="link">
          <h1>Products</h1>
        </Link>
        <Link to="/about" className="link">
          <h1>About</h1>
        </Link>
      </nav>

      <div>
        <h3>{title}</h3>
      </div>
    </header>
  )
}

export default header
